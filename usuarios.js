$(document).ready(function () {

    //carregar tabela
        $.ajax({
            url: './produto',
            type: 'get'
        })
            .done(function (result) {
                result = JSON.parse(result);
                var list = "";
                result.forEach(row => {
                    list += `<tr class='row'>`;
                    list += `<td><a href="./produto/delete.php?id=${row.id}">Apagar</td>`;
                    Object.values(row).forEach(cell => {
                        list += "<td>"
                        list += cell;
                        list += "</td>"
                    });
                        
                    list += "</tr>";
                });

                $('#data').html(list);
            })
    //fim

    //editar e deletar
    $(document).on("click", '.row', function(el){
        $('.row').removeClass("selected");
        $(this).addClass("selected");

        let id = $(this).children()[1]
        let name = $(this).children()[2]
        let ean = $(this).children()[3]
        let color = $(this).children()[4]
        let price = $(this).children()[5]
        let quantity = $(this).children()[6]

        $('#id').val($(id).text());
        $('#name').val($(name).text());
        $('#ean').val($(ean).text());
        $('#color').val($(color).text());
        $('#price').val($(price).text());
        $('#quantity').val($(quantity).text());
    });
});