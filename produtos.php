<?php
require_once("sessao.php");
?>
<!DOCTYPE html>
<html lang="pt_BR">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Produtos</title>

    <link rel="stylesheet" type="text/css" href="./main.css" />
</head>

<body>
    <?php
    require_once("menu.php");
    ?>
    <form action="./produto/save.php" class="form" id="formprodutcs" method="post">
        <label for="id">Código</label>
        <input type="text" id="id" name="id" readonly>
        <br>
        <label for="name">Nome</label>
        <input type="text" id="name" name="name" required>
        <br>
        <label for="ean">EAN</label>
        <input type="text" id="ean" name="ean" >
        <br>
        <label for="color">Cor</label>
        <input type="color" id="color" name="color">
        <br>
        <label for="price">Preço</label>
        <input type="number" min="0" id="price" name="price" required>
        <br>
        <label for="quantity">Quantidade</label>
        <input type="number" min="0" id="quantity" name="quantity">
        <br>
        <input type="submit" class="btn" value="Gravar">
    </form>
    <table class="table">
        <thead>
            <tr>
                <th>Ações</th>
                <th>ID</th>
                <th>Nome</th>
                <th>EAN</th>
                <th>cor</th>
                <th>preco</th>
                <th>quantidade</th>
            </tr>
        </thead>
        <tbody id="data"></tbody>
    </table>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="./produtos.js"></script>
</body>

</html>