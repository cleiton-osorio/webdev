$(document).ready(function () {

    //carregar tabela
        $.ajax({
            url: './cliente',
            type: 'get'
        })
            .done(function (result) {
                console.log(result);
                result = JSON.parse(result);
                var list = "";
                result.forEach(row => {
                    list += `<tr class='row'>`;
                    list += `<td><a href="./cliente/delete.php?cpf=${row.cpf}">Apagar</td>`;
                    Object.values(row).forEach(cell => {
                        list += "<td>"
                        list += cell;
                        list += "</td>"
                    });
                        
                    list += "</tr>";
                });

                $('#data').html(list);
            })
    //fim

    //editar e deletar
    $(document).on("click", '.row', function(){
        $('.row').removeClass("selected");
        $(this).addClass("selected");

        let cpf = $(this).children()[1]
        let nome = $(this).children()[2]
        let telefone = $(this).children()[3]
        let email = $(this).children()[4]

        $('#cpf').val($(cpf).text());
        $('#nome').val($(nome).text());
        $('#telefone').val($(telefone).text());
        $('#email').val($(email).text());
    });
});