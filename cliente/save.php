<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$cpf = $_POST["cpf"];
$nome = $_POST["nome"];
$telefone = $_POST["telefone"];
$email = $_POST["email"];

if (!$cpf || !$nome) {
    header('Location: ./../clientes.php');
    die;
}

require_once("./../connect.php");

/**
 * Gravar
 */
$sql = "INSERT INTO
`pessoa`(
    `cpf`,
    `nome`,
    `telefone`,
    `email`
    )
    VALUES(?,?,?,?)
    ON DUPLICATE KEY UPDATE
    `cpf` = VALUES(`cpf`),
    `nome` = VALUES(`nome`),
    `telefone` = VALUES(`telefone`),
    `email` = VALUES(`email`);
";
if ($stmt = mysqli_prepare($link, $sql)) {
    mysqli_stmt_bind_param(
        $stmt,
        'ssss',
        $cpf,
        $nome,
        $telefone,
        $email
    );
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_store_result($stmt);
    
    if ($result == 1) {
        header('Location: ./../clientes.php');
        die;
    } else if ($result > 1) {
        // echo ("2 ou mais registros foram retornados. Favor verificar.");
    } else {
        // echo "result outro";
        print_r($stmt->error_get_last);
    }
}
