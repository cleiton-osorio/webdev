<?php
session_start();
if($_SESSION['timer'] !== date('d/m/y')){
    session_destroy();
}
if(!isset($_SESSION['timer'])){
   header("Location: login.php");
}
?>