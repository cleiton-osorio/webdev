<?php
require_once("sessao.php");
?>
<!DOCTYPE html>
<html lang="pt_BR">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Clientes</title>

    <link rel="stylesheet" type="text/css" href="./main.css" />
</head>

<body>
    <?php
    require_once("menu.php");
    ?>
    <form action="./cliente/save.php" class="form" method="post">
        <label for="cpf">cpf</label>
        <input type="text" id="cpf" name="cpf" required>
        <br>
        <label for="nome">Nome</label>
        <input type="text" id="nome" name="nome" required>
        <br>
        <label for="telefone">telefone</label>
        <input type="text" id="telefone" name="telefone" >
        <br>
        <label for="email">email</label>
        <input type="email" id="email" name="email">
        <br>
        <input type="submit" class="btn" value="Gravar">
    </form>
    <table class="table">
        <thead>
            <tr>
                <th>Ações</th>
                <th>cpf</th>
                <th>Nome</th>
                <th>telefone</th>
                <th>email</th>
            </tr>
        </thead>
        <tbody id="data"></tbody>
    </table>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="./clientes.js"></script>
</body>

</html>