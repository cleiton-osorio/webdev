<?php

$id = $_GET["id"];

require_once("./../connect.php");

$sql = "DELETE FROM produto WHERE `id` = ?";

if ($stmt = mysqli_prepare($link, $sql)) {
    mysqli_stmt_bind_param($stmt,'i',$id);
    
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_store_result($stmt);
    
    if ($result == 1) {
        header('Location: ./../produtos.php');
        die;
    } else if ($result > 1) {
        // echo ("2 ou mais registros foram retornados. Favor verificar.");
    } else {
        // echo "result outro";
        print_r($stmt->error_get_last);
    }
}