<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

$name =     $_POST["name"];
$ean =      $_POST["ean"];
$color =    $_POST["color"];
$price =    valueIn($_POST["price"]);
$quantity = $_POST["quantity"];

if (!$name || !$price || !$quantity) {
    header('Location: ./../produtos.php');
    die;
}

require_once("./../connect.php");


if (isset($_POST["id"]) && !empty($_POST["id"])) {
    $id = $_POST["id"];
} else {
    $id = "";
}


/**
 * Gravar
 */
$sql = "INSERT INTO
`produto`(
    `id`,
    `name`,
    `ean`,
    `color`,
    `price`,
    `quantity`
    )
    VALUES(?,?,?,?,?,?)
    ON DUPLICATE KEY UPDATE
    `id` = VALUES(`id`),
    `name` = VALUES(`name`),
    `ean` = VALUES(`ean`),
    `color` = VALUES(`color`),
    `price` = VALUES(`price`),
    `quantity` = VALUES(`quantity`);
";
if ($stmt = mysqli_prepare($link, $sql)) {
    mysqli_stmt_bind_param(
        $stmt,
        'isssdi',
        $id,
        $name,
        $ean,
        $color,
        $price,
        $quantity
    );
    
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_store_result($stmt);
    
    if ($result == 1) {
        header('Location: ./../produtos.php');
        die;
    } else if ($result > 1) {
        // echo ("2 ou mais registros foram retornados. Favor verificar.");
    } else {
        // echo "result outro";
        print_r($stmt->error_get_last);
    }
}

function valueIn($value)
{
    $value = str_replace('.', '', $value);
    $value = str_replace(',', '.', $value);
    return $value;
}
